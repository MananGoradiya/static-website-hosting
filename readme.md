K1: How to install TYPO3 on CentOS

TYPO3 is a free and open-source Web content management system written in PHP. TYPO3 allows you to set up various permission levels for backend users. Also, this content management system allows you to manage multiple websites within one installation and share users, extensions, and content between them.

Step 1 – Install Apache, PHP, and MySQL
First, install the Epel repository:
yum -y install epel-release
Install Remi repository too:
yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm
After, enable PHP 7.2
yum-config-manager --enable remi-php72
Apache, PHP, and MySQL are needed to install TYPO3. You can install all of them by running this command:
yum -y install httpd mariadb mariadb-server php72  php php-zip php-intl php-gd php-json php-mysqli php-curl php-intl
-----------------------------------------------------------------
NOTE: If you have already installed any one this, then try to used this command for check
For PHP-> 
PHP Version check 
php –-version 
NOTE: If you don’t find any output, then you need to install PHP in your machine
for finding and Installation php packages 
yum whatprovides php
yum install php

For Apache->
Apache Version check 
httpd –v
NOTE: If you don’t find any output, then you need to install apache httpd in your machine
for finding and Installation httpd packages
yum whatprovides httpd
yum install httpd

For MariaDB->
mysql Version check 
mysql –-version
NOTE: If you don’t find any output, then you need to install MariaDB in your machine
for finding and installation of MariaDB packages
yum whatprovides mariadb
yum install mariadb-server
----------------------------------------
After Completing required installation, Now open php.ini file:
PHP Config File
vim /etc/php.ini
memory_limit set to a minimum of 256MB
max_execution_time set to a minimum 240 seconds
max_input_vars set to a minimum 1500
post_max_size set to 10M to allow uploads of 10MB
upload_max_filesize set to 10M to allow uploads of 10MB

After start MariaDB and Apache:
systemctl start mariadb httpd
And enable them:
systemctl enable mariadb httpd
for checking status		
systemctl status httpd
systemctl status mariadb

Step 2 – Create a database
for changing and config mysql
sudo mysql_secure_installation
You will need to create a database for TYPO3. Run this command to log in MySQL:
mysql -u root -p
After, run these commands to create a database:
CREATE DATABASE typo3 CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE USER 'typo3user'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON typo3.* TO 'typo3user'@'localhost';
FLUSH PRIVILEGES;
exit



Step 3 – Install TYPO3
Before installing, download the TYPO3 file. First, switch to a web directory:
cd /var/www/html
Download the TYPO3 file:
wget --content-disposition get.typo3.org/9
NOTE: If you find any error regarding wget, it’s because you need to install that in your virtual machine
yum whatprovides wget
yum install wget 

Extract the TYPO3 file:
tar -zxvf typo3_*.tar.gz
NOTE: If you find any error regarding tar, it’s because you need to install that in your virtual machine
yum whatprovides tar
yum install tar
Rename extracted directory to typo3:
mv typo3*/ typo3/
After run FIRST_INSTALL to install TYPO3:
touch /var/www/html/typo3/FIRST_INSTALL
And set apache permissions to TYPO3 file:
chown -R apache:apache /var/www/html/typo3



Step 4 – Finish install
Now, open any browser and go to http://youripaddress:port/typo3/. And click on No problems detected, continue with the installation button.
 
 
Complete this step by using the created database.
 
Select an database
 
Now create an administrative user
 
Open the TYPO3 Backend
 
Now log in to TYPO3
 
Dashboard

Conclusion
You have successfully installed TYPO3 on CentOS.
Enjoy!!










TASK2: Host a static website on the apache(httpd) server and check on the browser. Site must be located at the user end

STEP1: Create the layout for each domain
This example creates two domains, domain1.com and domain2.com.
Because the default permissions only enable you, the demo user, to browse your home folder, you need to grant httpd access to this folder as well by running the following command:
chmod 755 /home/demo
You can now create the basic layout for each domain. In your home directory, create a public_html folder by running the following command:
mkdir /home/demo/public_html
For each domain that you want to host, create a folder with a standard set of subfolders, as shown in the following examples:
mkdir -p /home/demo/public_html/domain1.com/{public,private,log,backup}
mkdir -p /home/demo/public_html/domain2.com/{public,private,log,backup}
NOTE: These commands create the folders public, private, log, and backup for each of your domains.log

Add public content for the website at domain1.com
This example creates a very simple HTML file in the public folder that enables you to quickly check that the virtual host works.
Run the following command to add content for domain1.com:
vim /home/demo/public_html/domain1.com/public/index.html
Enter code that is similar to the following example into the file:

<html>
  <head>
    <title>domain1.com</title>
  </head>
  <body>
    <h1>domain1.com</h1>
  </body>
</html>
Repeat the process so that you have a similar HTML index file for domain2.com.
Note: Ensure that you change the content in the index.html file to show domain2.com and not domain1.com.

STEP2: Add your virtual hosts to the virtual file
Edit the virtual file to add domain1.com by running the following command:
vim /etc/httpd/conf.d/domain1.com.conf
Add the following code to the file:
<VirtualHost *:80>
ServerName www.domain1.com
ServerAlias domain1.com
DocumentRoot /home/demo/public_html/domain1.com/public
ErrorLog /home/demo/public_html/domain1.com/log/error.log
CustomLog /home/demo/public_html/domain1.com/log/requests.log combined
</VirtualHost>
NOTE: Now add same code for domain2.com.conf, just change domain1 to domain2

                                                                              OR						
STEP2: Add your virtual hosts to the virtual file
To do virtual host,we can also do it by creating 2 directories, namely sites-available and sites-enabled.
EG      /etc/httpd/sites-available
EG)  /etc/httpd/sites-enabled

Now we need to create this virtual host file inside sites-enabled directory and then link it with sites-available directory using the following command. 
Now inside the sites-enabled directory create conf file 

vim /etc/httpd/sites-enabled/domain1.com.conf
<VirtualHost *:80>
ServerName www.domain1.com
ServerAlias domain1.com
DocumentRoot /home/demo/public_html/domain1.com/public
ErrorLog /home/demo/public_html/domain1.com/log/error.log
CustomLog /home/demo/public_html/domain1.com/log/requests.log combined
</VirtualHost>

Now link the directory sites-enabled with sites-available
ln -s /etc/httpd/sites-available/domain1.com.conf /etc/httpd/sites-enabled/domain1.com.conf







STEP3: Modify your hosts file 
Modifying your hosts file causes your local machine to look directly at the Internet Protocol (IP) address that you specify. Modifying the hosts file involves adding two entries to it. Each entry contains the IP address to which you want the site to resolve and a version of the Internet address. For example, add the following two entries points, www.domain1.com and domain2.com
If you are using a Microsoft operating system like Windows 10, Windows 8, Windows 7, and Windows Vista you must run Microsoft Notepad as an administrator.
Windows 10 and Windows 8
Use the following instructions if you’re running Windows 10 or Windows 8:
Press the Windows key.
Type Notepad in the search field.
In the search results, right-click Notepad and select Run as administrator.
From Notepad, open the following file:
c:\Windows\System32\Drivers\etc\hosts
Make the necessary changes to the file.
Select File > Save to save your changes.
Edit hosts file by, 
ip.addr 	domain1.com domain2.com

STEP4: Reload httpd
To enable your site, reload httpd by running the following command:
systemctl reload httpd



 STEP5: View your website
Navigate to your domain by pasting the following URL into your web browser’s navigation bar:
https://www.domain1.com:port
NOTE: You should see a simple test web page that displays the information in the index.html file.

Check the logs
In a previous step, you added code to the domain1.com.conf file that configured the locations of the logs. Run the following commands to check that there are now logs in that location:
ls /home/demo/public_html/domain1.com/log/
...
access.log error.log


